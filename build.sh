#!/bin/sh
dpkg-buildpackage -rfakeroot -uc -us -sa
REMOVE_LIST='debian/.debhelper 	debian/files debian/lmle-desktop debian/lmle-desktop.debhelper.log debian/lmle-desktop.substvars debian/lmle-hp-laptop debian/lmle-hp-laptop.debhelper.log debian/lmle-hp-laptop.substvars'
for F in $REMOVE_LIST ; do rm -fr "$F" ; done
